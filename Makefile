R6RS_DIR=$(HOME)/opt/lib/r6rs

all:
	echo "mk-r6rs"

install:
	mkdir -p $(R6RS_DIR)/mk-r6rs
	cp -f mk-r6rs.sls $(R6RS_DIR)/mk-r6rs/core.sls

push-all:
	r.gitlab-push.sh mk-r6rs
