mk-r6rs
-------

utility for making
[r6rs](http://r6rs.org/)
scheme libraries

This simple r6rs library provides the function
`mk-r6rs` to generate an r6rs library file from a
set of scheme files, determining an export list and
concatenating all definitions into a single library
s-expression.

© [rohan drape](http://rohandrape.net),
  2008-2022,
  [gpl](http://gnu.org/copyleft/)
